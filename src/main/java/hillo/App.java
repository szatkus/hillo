package hillo;

import def.js.String;

import static def.dom.Globals.requestAnimationFrame;

import java.util.ArrayList;

import def.dom.CanvasRenderingContext2D;
import def.dom.HTMLCanvasElement;
import def.dom.HTMLElement;

import static def.dom.Globals.console;
import static def.dom.Globals.document;


public class App {
    public static void main(String[] args) {
        HTMLCanvasElement canvas =  (HTMLCanvasElement) document.getElementById("output");
        CanvasRenderingContext2D context = (CanvasRenderingContext2D) canvas.getContext("2d");
        requestAnimationFrame(time -> {
            console.log(time);
            ArrayList l = new ArrayList();
            context.fillRect(0, 0, 30, 30);
        });
    }
}
